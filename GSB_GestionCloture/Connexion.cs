﻿
namespace GSB_GestionCloture
{
    using System.Configuration;
    using System.Data.SqlClient;

    /// <summary>
    /// Classe d'accès aux données.
    /// </summary>
    public class Connexion
    {
        private SqlConnection cnMySql;

        /// <summary>
        /// Initializes a new instance of the <see cref="Connexion"/> class.
        /// Initialise une nouvelle instance de la classe BDD.
        /// </summary>
        public Connexion()
        {
            ConnectionStringSettings cnString = ConfigurationManager.ConnectionStrings["StrConnGsb"];
            this.cnMySql = new SqlConnection(string.Format(
                        cnString.ConnectionString,
                        ConfigurationManager.AppSettings["SERVER"],
                        ConfigurationManager.AppSettings["DATABASE"],
                        ConfigurationManager.AppSettings["UID"],
                        ConfigurationManager.AppSettings["PWD"]));
        }

        /// <summary>
        /// Exécution d'une requête d'Administration passée en paramètre.
        /// </summary>
        /// <param name="query">Requête d'administration.</param>
        /// <param name="value">Valeurs associatives.</param>
        /// <param name="key">Clés associatives.</param>
        public void AdminQuery(string query, string[] value, string[] key)
        {
            this.cnMySql.Open();
            SqlCommand command = this.cnMySql.CreateCommand();
            command.CommandText = query;
            for (int i = 0; i <= key.Length - 1; i++)
            {
                command.Parameters.AddWithValue(key[i], value[i]);
            }

            command.ExecuteNonQuery();
            this.cnMySql.Close();
        }

        /// <summary>
        /// Exécution d'une requête de type Select passée en paramètre.
        /// </summary>
        /// <param name="query">Requête de type Select.</param>
        /// <returns>Résultats de la requête.</returns>
        public SqlDataReader SelectQuery(string query)
        {
            this.cnMySql.Open();
            SqlCommand command = new SqlCommand(query, this.cnMySql);
            SqlDataReader dataReader = command.ExecuteReader();
            command.Dispose();
            this.cnMySql.Close();
            return dataReader;
        }
    }
}
